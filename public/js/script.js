var app = angular.module('app', ['ngRoute', 'chart.js']);

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.
    when('/categories', {
        templateUrl: 'public/view/categories.html',
        controller: 'mainCategories'
    }).
    when('/subCategories/:name', {
        templateUrl: 'public/view/subCategories.html',
        controller: 'subCategories'
    }).
    when('/StartPoll/:name', {
        templateUrl: 'public/view/StartPoll.html',
        controller: 'feedback'
    }).
    when('/reports/:name', {
        templateUrl: 'public/view/reports.html',
        controller: 'reportcontroller'
    }).

    otherwise({
        redirectTo: '/categories'
    });
}]);

app.controller('mainCategories', function($scope, $http) {
    $http.get('https://jobportalapis.herokuapp.com/getallMaincategories').then(function(response) {
        $scope.mainCat = response.data.maincategory;
        console.log($scope.mainCat);
    });
});

app.controller('subCategories', function($scope, $http, $routeParams) {
    var cat = $routeParams.name;
    console.log(cat);
    $http.get('https://jobportalapis.herokuapp.com/getallSubcategories/' + cat).then(function(response) {
        $scope.subCat = response.data.subcategory;
        console.log($scope.subCat);
    });
});


app.controller('feedback', function($scope, $http, $routeParams, $location, $window) {
    $scope.addfeedback = function() {
            var cat = $routeParams.name;

            $window.localStorage.setItem("mobile", $scope.fb.mobile);
            var coin = $window.localStorage.getItem("coin");
            if (coin == undefined || coin == '') {
                coin = 1;
            } else {
                coin = parseInt(coin) + 1;
            }
            $window.localStorage.setItem("coin", coin);
            console.log(cat);
            console.log(this.fb)

            var tc = $scope.fb.basicost;
            var roi = $scope.fb.roi;
            var c = 100;
            var per = (roi / tc) * 100;
            console.log("per", +per);

            var feedback = JSON.stringify({
                feedback: {
                    mobile: $scope.fb.mobile,
                    basicost: $scope.fb.basicost,
                    roi: $scope.fb.roi,
                    comment: $scope.fb.comment,
                    roi_in_per: per
                }
            });

            var config = {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            var res = $http.post('https://jobportalapis.herokuapp.com/addfeedback/' + cat, feedback, config);
            res.success(function(feedback, status, headers, config) {
                $scope.message = feedback;

                document.getElementById("form1").reset();
                $.notify("You Have Posted FeedBack SucessFully!,You Got One More Coin <br>" +
                    "you have " + coin + " coin(s)", { align: "right", verticalAlign: "top", type: "info", delay: 5000 });
            });
            res.error(function(feedback, status, headers, config) {

            });
            // Making the fields empty
            console.log(feedback);

        },
        $scope.addReport = function() {
            console.log("here")
            var cat = $routeParams.name;
            console.log(cat);
            $location.path('/reports/' + cat);

        }

});

app.controller('reportcontroller', function($scope, $http, $routeParams) {

    var cat = $routeParams.name;
    console.log(cat);
    $http.get('https://jobportalapis.herokuapp.com/getFeedback/' + cat).then(function(response) {
        $scope.movie = response.data.feedback;
        var number = 1;
        $('#list').DataTable({
            data: response.data.feedback,
            destroy: true,
            retrieve: true,
            paging: true,
            columns: [
                { data: 'mobile', defaultContent: "<i>Not found</i>" },
                { data: 'basicost', defaultContent: "<i>Not found</i>" },
                { data: 'roi', defaultContent: "<i>Not found</i>" },
                { data: 'comment', defaultContent: "<i>Not found</i>" }

            ]
        });
        console.log(response);
    });
    $http.get('https://jobportalapis.herokuapp.com/getpositive/' + cat).then(function(response) {

        $scope.positive = response.data.feedback;
        console.log($scope.positive);
        $scope.data = [$scope.positive, $scope.negative, $scope.average];

    });

    $http.get('https://jobportalapis.herokuapp.com/getnegative/' + cat).then(function(response) {
        $scope.negative = response.data.feedback;
        console.log($scope.negative);
    });

    $http.get('https://jobportalapis.herokuapp.com/getaverage/' + cat).then(function(response) {
        $scope.average = response.data.feedback;
        console.log($scope.average);
    });

    $scope.labels = [" Well Satisfied feedback", "Satisfied feedback", "Unsatisfied feedback"];

});