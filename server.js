var http = require('http');
var port = process.env.PORT || 1337;

var path = require("path");
var express = require('express');
var app = express();

app.use('/asset', express.static(__dirname + '/asset'));
app.use('/public', express.static(__dirname + '/public'));
app.use('/js', express.static(__dirname + '/js'));
app.use('public/view/', express.static(__dirname + 'public/view/'));

app.use('/node_modules/', express.static(__dirname + '/node_modules/'));
app.get('/', function(req, res) {
    //  console.log(path.join(__dirname + '/Index.html'));
    res.sendFile(path.join(__dirname + '/index.html'));
    //__dirname : It will resolve to your project folder.
});

app.listen(port, function(req, res) {
    console.log("listening on port" + port);
});

// http.createServer(function(req, res) {

//     console.log("listening on port" + port);
//     // res.end('Hello World\n');
// }).listen(port);